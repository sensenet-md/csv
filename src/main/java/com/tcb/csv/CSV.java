package com.tcb.csv;

import java.util.List;
import java.util.Optional;

public interface CSV {
	public List<String> getHeader();
	public List<List<String>> getRows();
	public List<List<String>> getColumns();
	public List<String> getColumnByName(String name);
}
