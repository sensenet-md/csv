package com.tcb.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.tcb.common.exception.NonSymmetricArrayException;

public abstract class AbstractCSVTest {
	protected List<List<String>> lines;
	protected List<String> header;
	protected List<String> dataLine1;
	protected List<String> dataLine2;
	protected List<List<String>> dataColumns;
	protected List<String> dataColumn1;
	protected List<String> dataColumn2;
	protected List<String> dataColumn3;
	protected List<String[]> linesInput;
	
	protected abstract CSV createCSV(List<String[]> lines);
	
	@Before
	public void setUp() throws Exception {
		this.header = Arrays.asList("Col1","Col2","Col3");
		this.dataLine1 = Arrays.asList("a","b","c");
		this.dataLine2 = Arrays.asList("d","e","f");
		this.lines = Arrays.asList(header,dataLine1,dataLine2);
		this.linesInput = lines.stream()
				.map(l -> l.toArray(new String[0]))
				.collect(Collectors.toList());
		this.dataColumn1 = Arrays.asList(dataLine1.get(0),dataLine2.get(0));
		this.dataColumn2 = Arrays.asList(dataLine1.get(1),dataLine2.get(1));
		this.dataColumn3 = Arrays.asList(dataLine1.get(2),dataLine2.get(2));
		this.dataColumns = Arrays.asList(dataColumn1,dataColumn2,dataColumn3);
		}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testNull(){
		CSV csv = createCSV(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmpty(){
		CSV csv = createCSV(new ArrayList<String[]>());
	}
	
	@Test(expected=NonSymmetricArrayException.class)
	public void testNonSymmetric(){
		String[] dataline1 = new String[]{"a"};
		CSV csv = createCSV(Arrays.asList(header.toArray(new String[0]),dataline1));
	}

	
}
