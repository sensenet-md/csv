package com.tcb.csv;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.csv.NoHeaderCSV;

public class NoHeaderCSVTest extends AbstractCSVTest {
	private CSV testCSV;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.testCSV = createCSV(linesInput);
		}		
		
	
	@Override
	protected CSV createCSV(List<String[]> lines) {
		return new NoHeaderCSV(lines);
	}
	
	
	@Test(expected=UnsupportedOperationException.class)
	public void testGetHeader() {
		testCSV.getHeader();
	}

	@Test
	public void testGetData() {
		List<List<String>> testData = testCSV.getRows();
			
		assertEquals(Arrays.asList(header,dataLine1,dataLine2),testData);
	}
	
	@Test
	public void testGetColumns() {
		List<List<String>> testData = testCSV.getColumns();
		List<List<String>> refColumns = Arrays.asList(
				Arrays.asList(header.get(0),dataLine1.get(0),dataLine2.get(0)),
				Arrays.asList(header.get(1),dataLine1.get(1),dataLine2.get(1)),
				Arrays.asList(header.get(2),dataLine1.get(2),dataLine2.get(2))
				);
		assertEquals(refColumns,testData);
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void testGetColumnByName(){
		List<String> testData = testCSV.getColumnByName("Col3");
		
		assertEquals(dataColumn3,testData);
	}

}
