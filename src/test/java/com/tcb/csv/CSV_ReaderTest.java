package com.tcb.csv;

import static org.junit.Assert.*;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;

public class CSV_ReaderTest {

	private CSV_Reader csvReader;
	private CSV csv;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		String path = classLoader.getResource("csv/CsvReaderAdapter/test.csv").getPath();
		this.csvReader = new CSV_Reader(path,",",true);
		this.csv = csvReader.getCSV();
	}
	
	@Test(expected=IOException.class)
	public void testConstructor() throws IOException {
		new CSV_Reader("non-existant-path.csv",",",true);
	}

	@Test
	public void shouldGetHeaders() {
		List<String> refHeaders = Arrays.asList("H1","H2","H3");
		try {
			List<String> testHeaders = csv.getHeader();
			assertEquals(refHeaders,testHeaders);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Should not happen.");
		}
		
	}

	@Test
	public void shouldGetValues() {
		List<String> refLine1 = Arrays.asList("A","1","1.1");
		List<String> refLine2 = Arrays.asList("B","2","2.2");
		List<List<String>> refLines = Arrays.asList(refLine1,refLine2);
		
		try {
			List<List<String>> testLines = csv.getRows();
			assertEquals(refLines,testLines);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Should not happen.");
		}
		
	}

}
