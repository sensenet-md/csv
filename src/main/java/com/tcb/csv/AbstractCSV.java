package com.tcb.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import com.tcb.common.util.ListFilter;
import com.tcb.common.util.MultiArrayUtil;

public abstract class AbstractCSV implements CSV {
	protected String[][] data;
	
	
	public AbstractCSV(List<String[]> lines){
		if(null==lines || lines.size()==0) throw new IllegalArgumentException("Argument may not be null or empty");
	}
		
	@Override
	public List<List<String>> getRows() {
		return MultiArrayUtil.doubleArrayToLists(data);
	}
	
	@Override
	public List<List<String>> getColumns(){
		String[][] transposedData = MultiArrayUtil.transpose(data);
		return MultiArrayUtil.doubleArrayToLists(transposedData);
	}
	
	@Override
	public List<String> getColumnByName(String name){
		List<String> header = getHeader();
		List<List<String>> matchingColumns = new ArrayList<List<String>>();
		List<List<String>> columns = getColumns();
		for(int i=0;i<header.size();i++){
			String h = header.get(i);
			if(h.equals(name)) matchingColumns.add(columns.get(i));
		}
		try{
			return ListFilter.singleton(matchingColumns).get();
		} catch(NoSuchElementException e){
			throw new IllegalArgumentException(
					String.format("Could not find column with specified name: %s",name));
		}
	}
	
}
