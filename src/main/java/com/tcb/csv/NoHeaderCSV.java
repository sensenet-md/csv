package com.tcb.csv;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.common.util.MultiArrayUtil;

public class NoHeaderCSV extends AbstractCSV implements CSV{
	
	public NoHeaderCSV(List<String[]> lines){
		super(lines);
		String[][] raw = lines.toArray(new String[0][0]);
		MultiArrayUtil.checkSymmetric(raw);
		fillData(raw);
	}

	private void fillData(String[][] raw) {
		this.data = raw.clone();
	}

	@Override
	public List<String> getHeader() {
		throw new UnsupportedOperationException("CSV was not specified with a header.");
	}

}
