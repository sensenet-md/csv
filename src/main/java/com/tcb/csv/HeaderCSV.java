package com.tcb.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.common.util.MultiArrayUtil;

public class HeaderCSV extends AbstractCSV implements CSV {
	private String[] header;
		
	public HeaderCSV(List<String[]> lines) throws IllegalArgumentException {
		super(lines);
		String[][] raw = lines.toArray(new String[0][0]);
		MultiArrayUtil.checkSymmetric(raw);
		fillHeader(raw);
		fillData(raw);
		}
	
	private void fillHeader(String[][] raw){
		this.header = raw[0];
	}
	
	private void fillData(String[][] raw){
		this.data = new String[raw.length-1][MultiArrayUtil.getSymmetricYindex(raw)];
		for(int x=1;x<raw.length;x++){
			this.data[x-1] = raw[x];
		}
	}
	
	
	@Override
	public List<String> getHeader(){
		return Arrays.asList(header);
	}

	
}
