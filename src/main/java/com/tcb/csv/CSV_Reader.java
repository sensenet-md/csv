package com.tcb.csv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CSV_Reader {
	private CSV csv;

	public CSV_Reader(String fileName, String delimiterRegex, boolean hasHeader, String removeRegex) throws IOException {
		List<String[]> lines   = Files.readAllLines(Paths.get(fileName)).stream()
				.map(s -> s.replaceAll(removeRegex, ""))
				.map(s -> s.split(delimiterRegex))
				.collect(Collectors.toList());
		if(hasHeader) this.csv = new HeaderCSV(lines);
		else 		  this.csv = new NoHeaderCSV(lines);
	}
	
	public CSV_Reader(String fileName, String delimiterRegex, boolean hasHeader) throws IOException {
		this(fileName,delimiterRegex,hasHeader,"[\"\']");
	}
	
	public CSV getCSV() {
		return csv;
	}

}
