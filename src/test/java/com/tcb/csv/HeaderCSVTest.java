package com.tcb.csv;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.csv.HeaderCSV;

public class HeaderCSVTest extends AbstractCSVTest{
	private HeaderCSV testCSV;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.testCSV = new HeaderCSV(linesInput);
	}
	
	@Override
	protected CSV createCSV(List<String[]> lines) {
		return new HeaderCSV(lines);
	}
	
	@Test
	public void testGetHeader() {
		List<String> testHeader = testCSV.getHeader();
		
		assertEquals(header,testHeader);
		}

	@Test
	public void testGetData() {
		List<List<String>> testData = testCSV.getRows();
		
		assertEquals(Arrays.asList(dataLine1,dataLine2),testData);
		}

	@Test
	public void testGetColumns() {
		List<List<String>> testData = testCSV.getColumns();

		assertEquals(dataColumns,testData);
	}
	
	@Test
	public void testGetColumnByName(){
		List<String> testData = testCSV.getColumnByName("Col3");
		
		assertEquals(dataColumn3,testData);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetColumnByNameWhenInvalid(){
		List<String> testData = testCSV.getColumnByName("Col4");
	}

	
}
